#Importando bibliotecas para envio de email e geração de tabela HTML. 
from smtplib import SMTP
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import pandas as pd
from pretty_html_table import build_table
#Importando bibliotecas da versão 2.0
from datetime import date
#Importando bibliotecas do programa inicial
import PySimpleGUI as sg
import pandas as pd
import os.path
#Importando bibliotecas para executável windows
import fsspec
import xlrd
import openpyxl

data_atual = date.today()
hoje = data_atual.strftime('%d/%m/%Y')

class Email:
    def __init__(self, endereco, senha, corpo):
        self.endereco = endereco
        self.senha = senha
        self.corpo = corpo
    
    def envio_mail(self):
        destino = ['<financeiro@prinse.com.br>', '<comercial@prinse.com.br>', '<cgr@prinse.com.br>']
        message = MIMEMultipart()
        message['Subject'] = ('Atividade do dia ' + hoje)
        message['From'] = self.endereco
        message['To'] = ', '.join(destino)

        conteudo = ('Segue atividades realizadas: \n\n' + self.corpo + '\n\n Att. ')
        message.attach(MIMEText(conteudo, "html"))
        msg_body = message.as_string()

        server = SMTP('smtp.prinse.com.br', 587)
        server.starttls()
        server.login(message['From'], self.senha)
        server.sendmail(message['From'], destino, msg_body)
        server.quit()


def add_lin(df, lin):
    df.loc[-1] = lin
    df.index = df.index + 1  
    return df.sort_index()


def maiuscula(dados):
    for i in range(len(dados)):
        dados[i] = dados[i].upper()
    

colunas = "ATIVIDADE OS CLIENTE DATA PLANO ROTEADOR".split()
atvd_diaria = pd.DataFrame(columns=(colunas))
if (os.path.isfile("c://db_diaria.xlsx")):
        comp_atvd1 = pd.read_excel("c://db_diaria.xlsx")
else:
    comp_atvd1 = pd.DataFrame(columns=(colunas))   

print('\n\n')
print("Bem vindo ao Relatório Generator 2000!")
print('\n')
print('Pressione "Gravar" para registrar as atividades.')
print('Pressione "Sair" para importar os dados e encerrar o programa.')


layout = [
    [sg.Text('ATIVIDADES DIÁRIAS')],
    [sg.Text('ATIVIDADE', size=(15,1)), sg.InputText()],
    [sg.Text('O.S.', size=(15, 1)), sg.InputText()],
    [sg.Text('CLIENTE', size=(15, 1)), sg.InputText()],
    [sg.Text('DATA', size=(15, 1)), sg.InputText()],
    [sg.Text('PLANO', size=(15, 1)), sg.InputText()],
    [sg.Text('ROTEADOR', size=(15, 1)), sg.InputText()],
    [sg.Text('Dados para envio:')],
    [sg.Text('login:', size=(15, 1)), sg.InputText()],
    [sg.Text('senha:', size=(15, 1)), sg.InputText()],
    [sg.Button('Gravar'), sg.Button('Sair')]   
]

window = sg.Window('RELATÓRIO GENERATOR 2000', layout)

while True:             #Loop
    event, values = window.read()
   
    if event == 'Gravar':
        dados = [values[0], values[1], values[2], values[3], values[4], values[5]]
        maiuscula(dados)
        add_lin(atvd_diaria, dados)
        print('\n')
        print("Registro Realizado...")
        print(dados)
    
    if event in (sg.WIN_CLOSED, 'Sair'):
        comp_atvd1 = comp_atvd1.append(atvd_diaria)
        conta_mail = values[6]
        senha_mail = values[7]
        saida = build_table(atvd_diaria, 'blue_light')
        email = Email(conta_mail, senha_mail, saida)
        email.envio_mail()
        atvd_diaria.to_excel("c://atividade_diaria.xlsx", index=False)
        comp_atvd1.to_excel("c://db_diaria.xlsx", index=False)
        break

window.close()

